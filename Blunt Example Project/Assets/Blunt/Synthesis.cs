﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Blunt
{
	namespace Synthesis
	{
		using DSPType = System.Single;
		using SystemFloat = System.Single;

		public class EnvironmentTuning
		{
			/// <summary>
			/// The reference pitch for all tunings.
			/// </summary>
			public static DSPType basePitch = 440.0f;
			/// <summary>
			/// A power of 2 will generally ensure divisions will fall exactly.
			/// </summary>
			public static int samplesPerBeat = 32768;
			/// <summary>
			/// The beats per minute, the general system runs in. You should sequence in multiples
			/// of this value.
			/// </summary>
			public static DSPType baseBPM;
			/// <summary>
			/// baseBPM / 60, beats per second.
			/// </summary>
			public static DSPType baseBPS;
			public static DSPType reciprocalBeatsPerBar = 0.25f;
			public static DSPType reciprocalDivsPerBar = 0.0625f * reciprocalBeatsPerBar;
			public static DSPType reciprocalDivsPerBeat = 0.0625f;
			public static int semiTones = 12;
			public static int A4 = noteFromComposite (Pitch.A, 4);
			/// <summary>
			/// This may seem like a weird place to put it. Yes.
			/// Volume levels below this should be flushed to zero.
			/// </summary>
			public static DSPType denormalFlush = MathExt.dbToFraction(-120);
			public static DSPType sampleRate;
			/// <summary>
			/// 1 / sampleRate
			/// How many seconds a single sample takes.
			/// </summary>
			public static DSPType deltaT;
			/// <summary>
			/// 2 * PI / sampleRate
			/// </summary>
			public static DSPType hzToRads;
			/// <summary>
			/// One full rotation at this frequency (radians) corrosponds to one beat.
			/// </summary>
			public static DSPType bpmFreq;
			public enum Pitch
			{
				C, 
				CSharp,
				D,
				DSharp,
				E,
				F, 
				FSharp,
				G,
				GSharp,
				A,
				ASharp,
				B
			}

            static public float[] sineLut512;

            public static int noteFromComposite(Pitch p, int octave)
			{
				return (octave - 1) * semiTones + (int)p;
			}

			public static DSPType pitchFromNote(int note)
			{
				return pitchFromNote (note, 0.0f);
			}


            /// <summary>
            /// Provides a multiplier relative to a fundamental for obtaining the frequency of the semitone.
            /// Tet12 tuning.
            /// Example:
            ///     float cfreq = pitchFromComposite(C, 5);
            ///     float tertz = pitchMultiplierFromSemitone(4);
            ///     float tertzfreq = cfreq * tertz;
            /// </summary>
            /// <param name="semitone"></param>
            /// <returns></returns>
            public static DSPType pitchMultiplierFromSemitone(DSPType semitone)
            {
                return (DSPType)(System.Math.Pow(2, semitone / 12));
            }

			public static DSPType pitchFromComposite(Pitch p, int octave)
			{
				return pitchFromNote(noteFromComposite(p, octave), 0.0f);
			}

			public static DSPType pitchFromNote(int note, DSPType cents)
			{
				return (DSPType)(basePitch * System.Math.Pow (System.Math.Pow(2, (1.0/12.0)), note - A4 + cents * 0.01));
			}

			public static DSPType radiansFromNote(int note, DSPType cents)
			{
				return radiansFromFrequency(pitchFromNote(note, cents));
			}

			public static DSPType radiansFromComposite(Pitch p, int octave, DSPType cents = 0)
			{
				return radiansFromNote(noteFromComposite(p, octave), cents);
			}


			public static DSPType radiansFromFrequency(DSPType frequency)
			{
				return (DSPType)(2 * Math.PI * frequency / sampleRate);
			}

			public static void updateFields()
			{
				double actualSampleRate = 44100 /*AudioSettings.outputSampleRate*/;

				baseBPM = (DSPType)(60 * actualSampleRate / samplesPerBeat);
				baseBPS = (DSPType)(actualSampleRate / samplesPerBeat);
				sampleRate = (DSPType)actualSampleRate;
				deltaT = (DSPType)(1.0 / actualSampleRate);
				hzToRads = (DSPType)(2 * Math.PI / actualSampleRate);
				bpmFreq = (DSPType)((actualSampleRate / samplesPerBeat) * 2 * Math.PI / actualSampleRate);

                //Sound.AudioConfiguration.instance().updateEverything();
                // initialize lut
                sineLut512 = new float[514];
                for (int i = 0; i < sineLut512.Length; ++i)
                {
                    sineLut512[i] = (float)Math.Sin((2 * Math.PI * i) / 512);
                }
            }

			

		}


		public class WhiteNoise
		{
			// http://www.firstpr.com.au/dsp/pink-noise/
			// (it isn't pink, but white.)
			public uint[] seed = new uint[4];

			public WhiteNoise()
			{
				randomize();
			}
			public void randomize()
			{
				seed[0] = (uint)Blunt.Utils.SafeSystemRandom.getNextInteger();
				seed[1] = (uint)Blunt.Utils.SafeSystemRandom.getNextInteger();
				seed[2] = (uint)Blunt.Utils.SafeSystemRandom.getNextInteger();
				seed[3] = (uint)Blunt.Utils.SafeSystemRandom.getNextInteger();
			}
			/// <summary>
			/// reference code, do not use.
			/// </summary>
			public float noise(float volume)
			{
				seed[0] = seed[0] * 196314165 + 907633515;
				uint phase = seed[0] >> 9;
				phase |= 0x40000000;
				float ret = BitConverter.ToSingle(BitConverter.GetBytes(phase), 0) - 3.0f;
				return ret * volume;
            }

		};

		namespace Filters
		{

            public struct OnePole
            {
                public DSPType a0, b1, z1;

                public void designLP(DSPType cutoff)
                {
                    b1 = (DSPType)Math.Exp(-2.0 * Math.PI * cutoff * EnvironmentTuning.deltaT);
                    a0 = (DSPType)(1.0) - b1;
                }

                public void designHP(DSPType cutoff)
                {
                    //http://www.earlevel.com/main/2012/12/15/a-one-pole-filter/
                    throw new NotImplementedException();
                }

                DSPType process(DSPType s)
                {
                    return z1 = s * a0 + z1 * b1;
                }
            }

			// standard complex rotation
			public struct ComplexResonator
			{
				public DSPType cosine;
				public DSPType sine;
				public DSPType x;
				public DSPType y;
			};

			// simple Goertzel-like IIR filter
			public struct SimpleOscillator
			{
				public DSPType coeff;
				public DSPType d1, d2;
				public DSPType omega;
			};

			public class SmoothStep
			{
				/// <summary>
				/// The starting value.
				/// </summary>
				public DSPType goal;
				/// <summary>
				/// The phase increment. This is equal to 1 / numSamples to reach the goal.
				/// </summary>
				public DSPType omega;
				/// <summary>
				/// The current progress, in samples.
				/// </summary>
				public int deltaSamples;

				/// <summary>
				/// Only used for sanity checking.
				/// Optional use.
				/// </summary>
				public DSPType currentValue;

				/// <summary>
				/// Only used for sanity checking.
				/// Optional use.
				/// </summary>
				public DSPType startingValue;

				/// <summary>
				/// Designs a smoothstep filter. It will take at least 4 samples to reach the 
				/// destination, and steps will be quantized to 4 (downwards).
				/// </summary>
				/// <param name="endValue"></param>
				/// <param name="smoothTimeInMS"></param>
				/// <returns></returns>
				static public SmoothStep design(DSPType endValue, DSPType smoothTimeInMS)
				{
					SmoothStep ret = new SmoothStep();
					int numSamples = (int)(smoothTimeInMS * 0.001f * EnvironmentTuning.sampleRate);
					// drop negatives, quantize to 4, and set minimum to four.
					numSamples -= numSamples & 0x3;
					if(numSamples <= 0)
						numSamples = 4;

					ret.deltaSamples = 0;
					ret.goal = endValue;
					ret.omega = (float)(1.0 / numSamples);

					return ret;
				}
				/// <summary>
				/// Designs a SmoothStep filter, that immediately reaches its
				/// end value.
				/// </summary>
				/// <param name="value"></param>
				/// <returns></returns>
				static public SmoothStep constant(DSPType value)
				{
					SmoothStep ret = new SmoothStep();
					ret.deltaSamples = 4;
					ret.goal = value;
					ret.omega = 0.25f;

					return ret;
				}

				/// <summary>
				/// Example usage - do not use.
				/// </summary>
				/// <param name="s"></param>
				/// <returns></returns>
				static public void process(SmoothStep s, ref float value)
				{
					// okay, the point of this class is to support
					// continuous smoothing to some everchanging goal.
					// the catch is, the smoothstep filter may be updated from 
					// another thread meanwhile, but we still want it lock-free
					// this is why we unhinged the actual output value from the filter.
					// therefore, we start off copying the reference atomically to a local
					// variable. obviously it doesn't make sense in this case, however follow
					// this pattern for more complex code.
					SmoothStep filter = s;

					// find the fractionate argument to the smoothstep function.
					// we stop a 1.0f
					DSPType fraction = Mathf.Min((filter.deltaSamples + 1) * filter.omega, 1.0f);

					// calculate the smoothstep function:
					DSPType ss = (fraction) * (fraction) * (3.0f - 2.0f * (fraction));

					// output the value:
					value = filter.goal * ss + value * (1.0f - ss);

					// now here's the trick:
					// we store the progress in the filter, HOWEVER if it was updated to something
					// else, we update the old filter (which you ensured doesn't point the same place!!)
					filter.deltaSamples++;
				}
			};


			public struct SVFBiquad
			{
				// credits to Mystran for this filter he made in 15 minutes.
				// http://www.kvraudio.com/forum/viewtopic.php?f=33&t=348626
				public DSPType f, r, g;
				/// <summary>
				/// 0 ... 1
				/// </summary>
				public DSPType normalizedFrequency;
				/// <summary>
				/// 0.1 .. 10
				/// </summary>
				public DSPType Q;
				public DSPType z1, z2;

				public void design(DSPType centerFrequencyInHz, DSPType filterQ)
				{
					filterQ = Mathf.Clamp(filterQ, 0.1f, 20f);
					normalizedFrequency = 2 * centerFrequencyInHz / EnvironmentTuning.sampleRate;
                    f = Mathf.Tan(MathExt.fPiHalf * normalizedFrequency);
					r = f + 1 / filterQ;
					g = 1 / (f * r + 1);
					Q = filterQ;
                }
				/// <summary> only for reference. </summary>
				public DSPType lowpass(DSPType sample)
				{
					DSPType hp = (sample -r * z1 - z2) *g;
					DSPType bp = z1 + f * hp;
					DSPType lp = z2 + f * bp;
					z1 += 2 * f * hp;
					z2 += 2 * f * bp;
					return lp;
                }
				/// <summary> only for reference. </summary>
				public DSPType bandpass(DSPType sample)
				{
					DSPType hp = (sample - r * z1 - z2) * g;
					DSPType bp = z1 + f * hp;
					DSPType lp = z2 + f * bp;
					z1 += 2 * f * hp;
					z2 += 2 * f * bp;
                    return bp;
				}
				/// <summary> only for reference. </summary>
				public DSPType highpass(DSPType sample)
				{
					DSPType hp = (sample - r * z1 - z2) * g;
					DSPType bp = z1 + f * hp;
					DSPType lp = z2 + f * bp;
					z1 += 2 * f * hp;
					z2 += 2 * f * bp;
					return hp;
				}
			};

			public struct TwoPoleFilter
			{
				public DSPType feedback, omega;
				/// <summary>
				/// 0...1
				/// </summary>
				public DSPType Q;
				public DSPType z1, z2;
				public void quickDesign(DSPType centerFrequencyInHz, DSPType Q)
				{
					var coeff = EnvironmentTuning.sampleRate;
					// polynomial approximation to sin:
					// -8 *(((x - sr/2)/sr)^2) + 2
					coeff = centerFrequencyInHz - coeff * 0.5f * EnvironmentTuning.deltaT;
					omega = -8 * coeff * coeff + 2;
					feedback = Q + Q / ((DSPType)1.0 - omega);
					this.Q = Q;
                }
				/// <summary>
				/// This design takes into account the exponential curve commonly associated with filter cutoffs.
				/// That is, the parameter 0-1 scales exponentially from 10 to sampleRate/2
				/// </summary>
				/// <param name="fractionateFrequency"></param>
				/// <param name="Q"></param>
				public void design01(DSPType fractionateFrequency, DSPType Q)
				{
					const float min = 10f;
					var top = EnvironmentTuning.sampleRate * 0.5f;
					var curve = MathExt.UnityScale.exp(fractionateFrequency, min, top);
					// convert back to a linear 0-1
					var lin = MathExt.UnityScale.Inv.linear(curve, min, top);

					design(lin * top, Q);

				}

				public void setFrequency(DSPType centerFrequencyInHz)
				{
					var coeff = EnvironmentTuning.sampleRate;
					// polynomial approximation to sin:
					// -8 *(((x - sr/2)/sr)^2) + 2
					coeff = centerFrequencyInHz - coeff * 0.5f * EnvironmentTuning.deltaT;
					omega = -8 * coeff * coeff + 2;
				}

				public void setQ(DSPType Q)
				{
					feedback = Q + Q / ((DSPType)1.0 - omega);
					this.Q = Q;
				}

				public void design(DSPType centerFrequencyInHz, DSPType Q)
				{
					omega = (DSPType)(2.0 * Math.Sin(Math.PI * centerFrequencyInHz / EnvironmentTuning.sampleRate));
					feedback = Q + Q / ((DSPType)1.0 - omega);
					this.Q = Q;
				}

				public TwoPoleFilter(DSPType centerFrequencyInHz, DSPType Q)
					: this()
				{
					design(centerFrequencyInHz, Q);
				}
			};
			public class Design
			{

				public static ComplexResonator designResonator(DSPType rotationSpeed, DSPType initialPhase)
				{
					ComplexResonator ret;
					ret.cosine = (DSPType)Math.Cos (rotationSpeed);
					ret.sine = (DSPType)Math.Sin (rotationSpeed);
					ret.x = (DSPType)Math.Cos(initialPhase);
					ret.y = (DSPType)Math.Sin (initialPhase);
					return ret;

				}
				public static ComplexResonator designResonator(DSPType frequency)
				{
					return designResonator ((DSPType)(2 * Math.PI * frequency / EnvironmentTuning.sampleRate), 0.0f);
				}

				public static SimpleOscillator designOscillator(DSPType frequency)
				{
					SimpleOscillator ret;
					double omega = 2 * Math.PI * frequency / EnvironmentTuning.sampleRate;
					ret.coeff = 2 * (DSPType)Math.Cos (omega);
					ret.d1 = (DSPType)Math.Cos (-omega - Math.PI * 0.5);
					ret.d2 = (DSPType)Math.Cos (-2 * omega - Math.PI * 0.5);
					ret.omega = (DSPType)omega;
					return ret;
				}

			};


		};

        /// <summary>
        /// An interface for recieving timed callbacks
        /// from a rendering synth.
        /// </summary>
        /// <typeparam name="Synth"></typeparam>
        public interface SynthSequenceCallback<Synth>
        {
            void onSequenceCallback(Synth s, PlayHead p);
        }

        /// <summary>
        /// The basic interface of synthesizers.
        /// </summary>
        public interface Synthesizer : Sound.SoundStage
        {
            PlayHead getPlayHead();
            void addVoice(Voice v);
            void removeVoice(Voice v);
            void setCutThreshold(DSPType cutoffInDBs);
            Voice createVoice(int midiNote, DSPType detuneInCents = 0);
            Voice createVoice();
        }

        /// <summary>
        /// A synthesizer which supports sequencing callbacks.
        /// </summary>
        /// <typeparam name="Synth"></typeparam>
        public interface SequencingSynthesizer<Synth> : Synthesizer
		{
            void addSequencer(SynthSequenceCallback<Synth> cb);
            void removeSequencer(SynthSequenceCallback<Synth> cb);

		}

        /// <summary>
        /// A composite class which manages a sequencing synthesizer, 
        /// a sequencer and a callback sequencer. The class instantiates these composites, and links them.
        /// This class is therefore ideal for top-level shared synthesizing sequencer systems (what you want).
        /// </summary>
        public class SequencerSystem<Synth> where Synth : SequencingSynthesizer<Synth>, new()
        {
            public Synth synth;
            public Sequencer<Synth> sequencer;
            public Sound.GenerativeMusic.CallbackSequencer<Synth> callbackSequencer;

            public SequencerSystem()
            {
                synth = new Synth();
                sequencer = new Sequencer<Synth>(synth);
                callbackSequencer = new Sound.GenerativeMusic.CallbackSequencer<Synth>(synth);
            }

            /// <summary>
            /// Adds the synth to the mixer channel (thereby also the sequencers)
            /// </summary>
            /// <param name="channel"></param>
            public void addToMixerChannel(Sound.Mixer.MixerChannel channel)
            {
                channel.addStage(synth);
            }
        }

		public class Voice
		{
			
			public DSPType pitchMultiplier = 1.0f;
			public DSPType vibrato = 1.0f;
			public DSPType volume = 0.5f;
			public DSPType masterVolume = 1.0f;
			public int midiNote = EnvironmentTuning.A4;
			public bool enabled = false;
            /// <summary>
            /// Use this to avoid the synth killing your voice when it is silent for too long.
            /// </summary>
            public bool dontKillOnSilence;
            public DSPType phase = 0.0f;
			public DSPType pan = 0.0f;
			public ADSREnvelope adsr;
			/// <summary>
			/// the amount of samples since a point in time, where this voice had a volume below threshold
			/// </summary>
			public int timeSinceLastThreshold; 
			public float timeAtPlay; // the point in time where this note was played
                                     //
            public Synthesizer instrument;
			/// <summary>
            /// Sets the pitch of the voice to the argument, given in hertz.
            /// </summary>
            /// <param name="frequency"></param>
			public virtual void setPitch(DSPType frequency)	{}

            public virtual void setPitch(int midiNote, DSPType cents)
			{
				var pitch = EnvironmentTuning.pitchFromNote(midiNote, cents);
				//UnityEngine.Debug.Log("Setting pitch: " + pitch);
                setPitch(pitch);
			}

			public Voice(Synthesizer s)
			{
                instrument = s;
				adsr = new ADSREnvelope();
				enabled = false;
			}

			public virtual void play(float volumeInDBs)
			{

			}

			public virtual void play()
			{

			}

			public virtual void release()
			{
				adsr.release();
			}
			
			public virtual void stop()
			{
				enabled = false;
				adsr.stop();

			}
			
			
		};

	}

}