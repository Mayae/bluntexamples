﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;

namespace Blunt.ImageSignalTransforms
{
    public enum ColourComponent
    {
        Red, Green, Blue
    }

    public struct FloatColour
    {
        public float red, green, blue;
        public void normalize()
        {
            float max = Mathf.Max(red, blue, green);
            red /= max;
            green /= max;
            blue /= max;
        }

        static public FloatColour operator * (FloatColour left, float scale)
        {
            left.red *= scale;
            left.green *= scale;
            left.blue *= scale;
            return left;
        }

        static public FloatColour operator + (FloatColour left, FloatColour right)
        {
            left.red += right.red;
            left.green += right.green;
            left.blue += right.blue;
            return left;
        }

    }
    public interface ImageCoeffs
    {
        void ensureTimeSizes(long size);
        void ensureFreqSizes(long size);
        long bufferSize { get; set; }
    }

    public class ColourImageCoeffs1D : ImageCoeffs
    {
        public CResizableContainer<double> red, green, blue;
        public CResizableContainer<double> ftRed, ftGreen, ftBlue;

        public ColourImageCoeffs1D()
        {
            red = new CResizableContainer<double>();
            green = new CResizableContainer<double>();
            blue = new CResizableContainer<double>();

        }

        /// <summary>
        /// Ensures the buffers in the time-domain are of at least this size.
        /// May not reallocate.
        /// </summary>
        /// <param name="size"></param>
        public void ensureTimeSizes(long size)
        {
            size = MathExt.nextPowTwoInclusive(size);
            red.ensureSize(size);
            green.ensureSize(size);
            blue.ensureSize(size);
        }

        /// <summary>
        /// Ensures the buffers in the freq-domain are of at least this size.
        /// May not reallocate.
        /// </summary>
        /// <param name="size"></param>
        public void ensureFreqSizes(long size)
        {
            size = MathExt.nextPowTwoInclusive(size);
            if (ftRed == null)
            {
                ftRed = new CResizableContainer<double>();
                ftGreen = new CResizableContainer<double>();
                ftBlue = new CResizableContainer<double>();
            }

            ftRed.ensureSize(size);
            ftGreen.ensureSize(size);
            ftBlue.ensureSize(size);
        }

        /// <summary>
        /// Note this only affects the time domain data.
        /// Frequency domain will automatically be sized (if needed, at all)
        /// </summary>
        public long bufferSize
        {
            get
            {
                return red.size();
            }
            set
            {
                ensureTimeSizes(value);
            }
        }
    }

    public class ImageCoeffs1D : ImageCoeffs
    {
        public CResizableContainer<double> intensity, ftIntensity;

        public ImageCoeffs1D()
        {
            intensity = new CResizableContainer<double>();

        }

        /// <summary>
        /// Ensures the buffers in the time-domain are of at least this size.
        /// May not reallocate.
        /// </summary>
        /// <param name="size"></param>
        public void ensureTimeSizes(long size)
        {
            size = MathExt.nextPowTwoInclusive(size);
            intensity.ensureSize(size);
        }

        /// <summary>
        /// Ensures the buffers in the freq-domain are of at least this size.
        /// May not reallocate.
        /// </summary>
        /// <param name="size"></param>
        public void ensureFreqSizes(long size)
        {
            size = MathExt.nextPowTwoInclusive(size);
            if (ftIntensity == null)
            {
                ftIntensity = new CResizableContainer<double>();
            }

            ftIntensity.ensureSize(size);
        }

        /// <summary>
        /// Note this only affects the time domain data.
        /// Frequency domain will automatically be sized (if needed, at all)
        /// </summary>
        public long bufferSize
        {
            get
            {
                return intensity.size();
            }
            set
            {
                ensureTimeSizes(value);
            }
        }
    }



    /// <summary>
    /// Keep this around. It is optimized for continous analysis.
    /// </summary>
    public class FourierTransform1D
    {
        /// <summary>
        /// Does a forward fourier transform of all three channels
        /// </summary>
        /// <param name="data"></param>
        public void forward3D(ColourImageCoeffs1D data, bool copy = true)
        {
            data.ensureFreqSizes(data.bufferSize * 2);

            var fr = data.ftRed.getData();
            var fg = data.ftGreen.getData();
            var fb = data.ftBlue.getData();
            // copy contents
            if (copy)
            {
                var r = data.red.getData();
                var g = data.green.getData();
                var b = data.blue.getData();

                for (int i = 0; i < data.bufferSize; i++)
                {
                    fr[i * 2] = r[i];
                    fg[i * 2] = g[i];
                    fb[i * 2] = b[i];
                    // only real-valued, set imaginary to zero.
                    fr[i * 2 + 1] = fg[i * 2 + 1] = fb[i * 2 + 1] = 0.0;
                }
            }

            fft.FFT(fr, true);
            fft.FFT(fg, true);
            fft.FFT(fb, true);
        }


        /// <summary>
        /// Does a forward fourier transform of all three channels
        /// </summary>
        /// <param name="data"></param>
        public void forward1D(ImageCoeffs1D data, bool copy = true)
        {
            data.ensureFreqSizes(data.bufferSize * 2);

            var fi = data.ftIntensity.getData();
            // copy contents
            if (copy)
            {
                var g = data.intensity.getData();

                for (int i = 0; i < data.bufferSize; i++)
                {
                    fi[i * 2] = g[i];
                    // only real-valued, set imaginary to zero.
                    fi[i * 2 + 1] = 0.0;
                }
            }

            fft.FFT(fi, true);
        }


        public FourierTransform1D()
        {
            fft = new Lomont.LomontFFT();
            fft.A = 1;
            fft.B = -1;
        }

        private Lomont.LomontFFT fft;
    }

    public class ColourAnalysis
    {

        public static ColourComponent majorColourComponent(ColourImageCoeffs1D image)
        {
            const int minimumBlobSize = 3;

            double[][] channels 
                = new double[][] { image.red.getData(), image.green.getData(), image.blue.getData() };

            double[] vRed = image.red.getData(), vGreen = image.green.getData(), vBlue = image.blue.getData();


            double[] score = { 0, 0, 0 };
            double[] acc = { 0, 0, 0 };
            int[] cont = { 0, 0, 0 };

            for (int i = 0; i < image.bufferSize; ++i)
            {

                acc[0] = vRed[i] - (vBlue[i] + vGreen[i]) * 0.5;
                acc[1] = vGreen[i] - (vBlue[i] + vRed[i]) * 0.5;
                acc[2] = vBlue[i] - (vRed[i] + vGreen[i]) * 0.5;
                // we have a deviation
                for(int c = 0; c < 3; ++c)
                {
                    if (acc[c] > 0)
                    {
                        cont[c]++;
                        // but is it part of a larger deviation?
                        if (cont[c] >= minimumBlobSize)
                        {
                            // yes, add score (and scale it by blob size):
                            score[c] += (1 + cont[c] - minimumBlobSize) * acc[c];
                        }
                    }
                    else
                    {
                        // reset..
                        cont[c] = 0;
                    }
                }

            }

            if(score[0] > score[1])
            {
                if(score[0] > score[2])
                {
                    return ColourComponent.Red;
                }
                else
                {
                    return ColourComponent.Blue;
                }
            }
            else
            {
                if(score[1] > score[2])
                {
                    return ColourComponent.Green;
                }
                else
                {
                    return ColourComponent.Blue;
                }
            }

        }


        public static FloatColour rgbDeviation(ColourImageCoeffs1D image)
        {
            double red = 0, green = 0, blue = 0;

            double scale = (1.0 / image.bufferSize);

            double[] vRed = image.red.getData(), vGreen = image.green.getData(), vBlue = image.blue.getData();

            for(int i = 0; i < image.bufferSize; ++i)
            {
                double acc;
                acc = vRed[i] - (vBlue[i] + vGreen[i]) * 0.5;
                red += acc > 0 ? acc * scale : 0;
                acc = vGreen[i] - (vRed[i] + vBlue[i]) * 0.5;
                green += acc > 0 ? acc * scale : 0;
                acc = vBlue[i] - (vRed[i] + vGreen[i]) * 0.5;
                blue += acc > 0 ? acc * scale : 0;
            }

            FloatColour ret;
            ret.red = (float)red;
            ret.green = (float)green;
            ret.blue = (float)blue;
            return ret;
        }
    }

    /// <summary>
    /// Transforms 2D images into 1D coefficients for each channel,
    /// based on a rectangular axis (subimage).
    /// </summary>
    public class ColourBoxFilterKernel1D
    {
        public struct BoxFilterSettings
        {
            /// <summary>
            /// ...
            /// </summary>
            public uint imageWidth;
            /// <summary>
            /// ...
            /// </summary>
            public uint imageHeight;
            /// <summary>
            /// A fraction, representing how much the kernel interpolation deviates from the center.
            /// -1 to 1
            /// </summary>
            public float offsetAxis;
            /// <summary>
            /// Normally, this kernel composites coefficients from the center bottom to the top.
            /// Use this to scan from left center to right center instead.
            /// </summary>
            public bool swapAxis;
            /// <summary>
            /// a fraction representing how much of the orthogonal axis to lowpass
            /// </summary>
            public float kernelSize;
        }

        public static void analyse(Color32[] image, ColourImageCoeffs1D coeffs, BoxFilterSettings settings)
        {
            unchecked
            {
                // how many coefficients we generate
                uint numCoeffs = (uint)coeffs.bufferSize;

                // the axises
                uint orthogonalAxis = settings.swapAxis ? settings.imageWidth : settings.imageHeight;
                uint axis = settings.swapAxis ? settings.imageHeight : settings.imageWidth;

                uint lengthAverage = System.Math.Max(1u, (uint)(orthogonalAxis * settings.kernelSize));

                // the stride spanning the image from edge to edge
                uint lengthStride = (orthogonalAxis) / numCoeffs;
                // the stride spanning the center and averaging some pixels around
                uint averageStride = MathExt.min((uint)lengthAverage, orthogonalAxis);

                uint axisStrideScaled = (uint)System.Math.Max((int)axis - (int)averageStride, 0);
                long offsetTemp = (long)System.Math.Round(axisStrideScaled * 0.5 + axisStrideScaled * settings.offsetAxis * 0.5);
                uint offset = (uint)MathExt.confineTo(offsetTemp, 0, axis - lengthAverage);

                double kernelScale = 1.0 / (lengthStride * averageStride);


                // loop over image, and lowpass/box filter it to obtain a single intensity value for each harmonic
                for (uint i = 0; i < numCoeffs; ++i)
                {

                    double red = 0;
                    double green = 0;
                    double blue = 0;

                    // calculate the volume from an interpolation kernel (box filter)
                    if (!settings.swapAxis)
                    {
                        for (uint y = 0; y < lengthStride; ++y)
                        {
                            uint yIndex = (y + i * lengthStride) * settings.imageWidth;
                            for (uint x = 0; x < averageStride; ++x)
                            {
                                uint index = offset + x + yIndex;
                                red += image[index].r;
                                green += image[index].g;
                                blue += image[index].b;
                            }

                        }
                    }
                    else
                    {
                        for (uint y = 0; y < lengthAverage; ++y)
                        {
                            uint yIndex = (y + offset) * settings.imageWidth;
                            for (uint x = 0; x < lengthStride; ++x)
                            {
                                uint index = (settings.imageWidth - 1u) - (x + i * lengthStride) + yIndex;
                                red += image[index].r;
                                green += image[index].g;
                                blue += image[index].b;

                            }
                        }

                    }

                    // can use IFFT here.
                    coeffs.red[i] = kernelScale * red / 0xFF;
                    coeffs.green[i] = kernelScale * green / 0xFF;
                    coeffs.blue[i] = kernelScale * blue / 0xFF;

                }

            }
        }

        public static void analyse(Color32[] image, ImageCoeffs1D coeffs, BoxFilterSettings settings)
        {
            unchecked
            {
                // how many coefficients we generate
                uint numCoeffs = (uint)coeffs.bufferSize;

                // the axises
                uint orthogonalAxis = settings.swapAxis ? settings.imageWidth : settings.imageHeight;
                uint axis = settings.swapAxis ? settings.imageHeight : settings.imageWidth;

                uint lengthAverage = System.Math.Min(1u, (uint)(orthogonalAxis * settings.kernelSize));

                // the stride spanning the image from edge to edge
                uint lengthStride = (orthogonalAxis) / numCoeffs;
                // the stride spanning the center and averaging some pixels around
                uint averageStride = MathExt.min((uint)lengthAverage, orthogonalAxis);


                long offsetTemp = (long)System.Math.Round(axis * 0.5 + axis * settings.offsetAxis * 0.5);
                uint offset = (uint)MathExt.confineTo(offsetTemp, 0, axis - lengthAverage);

                double kernelScale = 1.0 / (lengthStride * averageStride);


                // loop over image, and lowpass/box filter it to obtain a single intensity value for each harmonic
                for (uint i = 0; i < numCoeffs; ++i)
                {

                    double intensity = 0;

                    // calculate the volume from an interpolation kernel (box filter)
                    if (!settings.swapAxis)
                    {
                        for (uint y = 0; y < lengthStride; ++y)
                        {
                            uint yIndex = (y + i * lengthStride) * settings.imageWidth;
                            for (uint x = 0; x < averageStride; ++x)
                            {
                                uint index = offset + x + yIndex;
                                intensity += image[index].r + image[index].g + image[index].b;
                            }

                        }
                    }
                    else
                    {
                        for (uint y = 0; y < lengthAverage; ++y)
                        {
                            uint yIndex = (y + offset) * settings.imageWidth;
                            for (uint x = 0; x < lengthStride; ++x)
                            {
                                uint index = (settings.imageWidth - 1u) - (x + i * lengthStride) + yIndex;
                                intensity += image[index].r + image[index].g + image[index].b;

                            }
                        }

                    }

                    // can use IFFT here.
                    coeffs.intensity[i] = kernelScale * intensity / (0xFF * 3);

                }

            }
        }
    }
}
