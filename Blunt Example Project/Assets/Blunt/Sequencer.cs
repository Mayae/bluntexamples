﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios, jthorborg.com]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;

namespace Blunt.Synthesis
{
	public class Sequencer<Synth>
		: SynthSequenceCallback<Synth> where Synth : SequencingSynthesizer<Synth>
	{
		private Synth hostSynth;
        
        /// <summary>
        /// Represents a 'midi'-like note, playing a voice buffer through the synthesizer.
        /// </summary>
		public struct NoteInfo
		{
            /// <summary>
            /// The frequency in radians.
            /// </summary>
			public float pitch;
            /// <summary>
            /// Volume in decibels, full scale.
            /// </summary>
			public float velocity;
            /// <summary>
            /// Length in beats before this note is released.
            /// </summary>
			public float length;
            /// <summary>
            /// Should this note be played?
            /// </summary>
			public bool enabled;

            public NoteInfo(float frequency, float volume = 0, float lengthInBeats = 1)
            {
                pitch = frequency;
                velocity = volume;
                length = lengthInBeats;
                enabled = true;
            }
			static public NoteInfo empty()
			{
				NoteInfo ret;
				ret.pitch = ret.velocity = ret.length = 0;
				ret.enabled = false;
				return ret;
			}
		}

		public class SequencerToken
		{
			public GenericVoiceBuffer playableSound;
			public PlayHead.TimeOffset timeAtStart;
			public PlayHead.TimeOffset lengthToRelease;
			public PlayHead.TimeOffset lengthToNext;
			public Synthesis.Voice playingVoice = null;
			public bool isRecurrent;
			public System.Action onPlay;
			public System.Action onRelease;
			public NoteInfo info;

			public SequencerToken(GenericVoiceBuffer p, PlayHead.TimeOffset tap, PlayHead.TimeOffset tar)
			{
				playableSound = p;
				timeAtStart = tap;
				lengthToRelease = lengthToNext = tar;
				isRecurrent = false;
			}

			public void cancelRecurrence()
			{
				isRecurrent = false;
			}
		}



		public void playNotes(GenericVoiceBuffer voices, NoteInfo[] notes)
		{
			if(notes != null)
			{
				for(int i = 0; i < notes.Length; ++i)
				{
					var token = sequenceIn(voices, 0, notes[i].length);
					token.info = notes[i];
                }

			}
		}

		private System.Collections.Generic.LinkedList<SequencerToken> noteList;
		public Sequencer(Synth instrument)
		{
			noteList = new System.Collections.Generic.LinkedList<SequencerToken>();
			hostSynth = instrument;
			hostSynth.addSequencer(this);
		}

		public void onSequenceCallback(Synth synth, PlayHead ph)
		{
			var currentTime = ph.preciseTiming;
			//if(currentTime != 0.0)
			//	currentTime += ph.quantization;

			for(var it = noteList.First; it != null; it = it.Next)
			{
				float velocity = 0;
				SequencerToken s = it.Value;
				//Debug.Log(currentTime + " > " + s.timeAtStart.differenceInSeconds);
				// is voice played yet?
				if(s.playingVoice == null)
				{

					// no, see if it should be sequenced:
					if(currentTime >= s.timeAtStart.beats)
					{

						s.playingVoice = s.playableSound.stealOldestVoice();
						s.playingVoice.stop();
                        // does sound have additional info?
						if(s.info.enabled)
						{
							s.playingVoice.setPitch(s.info.pitch);
							velocity = s.info.velocity;
							if(s.onPlay != null)
								s.onPlay();
							s.playingVoice.play(velocity);
						}
						else
						{
							if(s.onPlay != null)
								s.onPlay();
							s.playingVoice.play();
						}

						//s.onPlay();
					}
				}
				else
				{
					// check if it should be released
					if(currentTime >= (s.timeAtStart + s.lengthToRelease).beats)
					{
						s.playingVoice.release();
						if(s.onRelease != null)
							s.onRelease();
					}
					// check if it should be stopped or played again.
					if(currentTime >= (s.timeAtStart + s.lengthToNext).beats)
					{
						s.playingVoice.release();
						if(s.onRelease != null)
							s.onRelease();
						// remove the entry:
						if(!s.isRecurrent)
						{
							noteList.Remove(it);
						}
						else
						{
							s.playingVoice = s.playableSound.stealOldestVoice();
							s.playingVoice.stop();
							if(s.onPlay != null)
								s.onPlay();
							s.playingVoice.play();
							s.timeAtStart.beats = currentTime;
						}
					}
				}
			}

		}

		public SequencerToken sequenceIn(GenericVoiceBuffer playableSound, PlayHead.TimeOffset when, PlayHead.TimeOffset length)
		{
			var now = hostSynth.getPlayHead().getCurrentOffset();
			var token = new SequencerToken(playableSound, now + when, length);
			noteList.AddLast(token);
			return token;
		}

		public SequencerToken sequenceEvery(GenericVoiceBuffer playableSound, PlayHead.TimeOffset offsetToStart, PlayHead.TimeOffset length)
		{
			var now = hostSynth.getPlayHead().getCurrentOffset();
			if(0 >= length.beats)
				throw new System.ArgumentException("You must specify a length greater than zero.");
			var token = new SequencerToken(playableSound, now + offsetToStart, length);
			token.isRecurrent = true;
			noteList.AddLast(token);
			return token;
		}
	}
}