﻿using UnityEngine;
using Blunt;
using Blunt.Sound;
using System.Collections;


public class AudioSystem_Noise : MonoBehaviour
{
	Mixer mixer;
	Mixer.MixerChannel channel;
	Noise noise;

	class Noise : SoundStage
	{
		Utils.CheapRandom r = new Utils.CheapRandom();

		public void process(float[] data, int nSampleFrames, int channels, bool channelIsEmpty)
		{
			for (int i = 0; i < nSampleFrames * channels; ++i)
			{
				data[i] += r.random11() * 0.05f;

			}
		}
	}

	void Awake()
	{
		mixer = GetComponent<Mixer>();
		noise = new Noise();
		// get the channel called noise (created if it doesn't exist)
		channel = mixer.getChannel("noise");
		// add our noise object to the chain.
		channel.addStage(noise);
		// enable the channel:
		channel.setIsEnabled(true);
		// and, enable the mixer:
		mixer.setIsEnabled(true);
	}

	void Start()
	{
		// start runs after awake. Enable the mixers sound system, and 
		mixer.enableSoundSystem();
	}

}