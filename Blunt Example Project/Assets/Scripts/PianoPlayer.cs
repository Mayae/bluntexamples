﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

#define V2

#define ADVANCED_SYNTH

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Synthesis;
using Synth = Blunt.Synthesis.FMSynth;


public class PianoPlayer : MonoBehaviour
{
	private Synth mainSynth;
	public int downs = 0;
	public int ups = 0;
	public int activeVoices;
	public int activeOperators;
	public Blunt.Sound.Mixer mixer;

	// Use this for initialization
	public int numberOfVoices;
	class Tangent
	{
		public KeyCode keyCode;
		public int note;
		public Blunt.CQueue<Synth.Voice> voices;
		public Tangent(KeyCode kc, int n)
		{
			keyCode = kc;
			note = n;
			voices = new Blunt.CQueue<Synth.Voice>();
		}
	};

	readonly Tangent[] tangents =
	{
		new Tangent( KeyCode.A, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.C, 4)),
		new Tangent( KeyCode.W, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.CSharp, 4)),
		new Tangent( KeyCode.S, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.D, 4)),
		new Tangent( KeyCode.E, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.DSharp, 4)),
		new Tangent( KeyCode.D, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.E, 4)),
		new Tangent( KeyCode.F, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.F, 4)),
		new Tangent( KeyCode.T, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.FSharp, 4)),
		new Tangent( KeyCode.G, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.G, 4)),
		new Tangent( KeyCode.Y, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.GSharp, 4)),
		new Tangent( KeyCode.H, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.A, 4)),
		new Tangent( KeyCode.U, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.ASharp, 4)),
		new Tangent( KeyCode.J, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.B, 4)),
		new Tangent( KeyCode.K, EnvironmentTuning.noteFromComposite (EnvironmentTuning.Pitch.C, 5))
	};

	private void OnGUI()
	{
		GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height) * 0.5f, new Vector2(Screen.width, Screen.height)), "Press A - K on the keyboard. Z and X shifts octaves");
	}

	class PlayableVoice
	{
		public readonly Synth.Voice voice;
		public Tangent associatedTangent;
		public Synth.Voice.Operator parameter;
		public PlayableVoice(Synth.Voice v)
		{
			voice = v;
			associatedTangent = null;
		}
	};
	private PlayableVoice[] voices;

	void Awake()
	{
		mainSynth = new Synth();
		var channel = mixer.getChannel("Main");

		channel.addStage(mainSynth);
		mixer.setIsEnabled(true);
		if(numberOfVoices < 1)
			numberOfVoices = 1;
		voices = new PlayableVoice[numberOfVoices];
		for(int i = 0; i < numberOfVoices; ++i)
		{
			voices[i] = new PlayableVoice((Synth.Voice)mainSynth.createVoice());
			var voice = voices[i].voice;
			Synth.Voice.Operator op;
#if ADVANCED_SYNTH

			op = voice.createAndAddOperator();
			op.isModulator = true;
			op.pitchRelation = 1f;
			op.envelope.setADSR(100, 0, 0, 100000);
			op.volume = 0.15f;

			op = voice.createAndAddOperator();
			op.envelope.setADSR(1, 900, -24, 2000);
			op.pitchRelation = 0.9975f;

			op = voice.createAndAddOperator();
			op.envelope.setADSR(1, 900, -24, 2000);
			op.pitchRelation = 2.0025f;
			op.mix = 0.5f;

			op = voice.createAndAddOperator();
			op.envelope.setADSR(0, 0, 0, 5000);
			op.op = FMSynth.Voice.Operator.Operation.Multiplicative;
			op.mix = 0.05f;
			op.isFixed = true;
			op.pitchRelation = 2;

			op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.D, 5);

			op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 3000, 4);
			op.modulationDepth = 0.7f;
			op.envelope.setADSR(5000, 500, -6, 10000);

#else
			op = voices[i].voice.createAndAddOperator();
			op.envelope.setADSR(1000, 5000, -24, 1000);
			op.mix = 1;
#endif
		}

	}

	PlayableVoice getAvailableOrOldestVoice()
	{
		int oldIndex = 0;
		float oldTime = float.MaxValue;

		for(int i = 0; i < voices.Length; ++i)
		{

			if(!voices[i].voice.enabled)
			{
				return voices[i];
			}
			else if(oldTime > voices[i].voice.timeAtPlay)
			{
				oldTime = voices[i].voice.timeAtPlay;
				oldIndex = i;
			}
		}

		// no free voices found, return the oldest:

		return voices[oldIndex];
	}

	void Update()
	{

		if(Input.GetKeyDown(KeyCode.Z))
		{
			for(int i = 0; i < tangents.Length; ++i)
			{
				// pitch 12 semitones down
				tangents[i].note -= 12;
			}

		}
		if(Input.GetKeyDown(KeyCode.X))
		{
			for(int i = 0; i < tangents.Length; ++i)
			{
				// pitch 12 semitones up
				tangents[i].note += 12;
			}

		}
		for(int i = 0; i < tangents.Length; ++i)
		{
			if(Input.GetKeyDown(tangents[i].keyCode))
			{
				var pv = getAvailableOrOldestVoice();
				pv.voice.setPitch(tangents[i].note, 0);
				pv.voice.stop();
				if(pv.associatedTangent != null)
				{
					pv.associatedTangent.voices.remove(pv.voice);
				}
				pv.associatedTangent = tangents[i];
				tangents[i].voices.push(pv.voice);
				pv.voice.play(-6f);
				downs++;

			}
			if(Input.GetKeyUp(tangents[i].keyCode))
			{
				var voice = tangents[i].voices.pop();
				// voice might have been stolen.
				if(voice != null)
				{
					voice.release();
				}
				//voices[i].release();
				ups++;
			}
		}

		if(Input.GetKeyDown(KeyCode.Q))
		{
			foreach(var voice in voices)
			{
				if(voice.parameter != null)
					voice.parameter.parameterSetGoal(Random.value, 500);
			}

		}
		activeVoices = mainSynth.numActiveVoices;
		activeOperators = mainSynth.numActiveOperators;
	}
}
