﻿using UnityEngine;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;
using System.Collections;


public class AudioSystem_SimplePoly : MonoBehaviour
{
	Mixer mixer;
	Mixer.MixerChannel channel;
	FMSynth synth;
	VoiceBuffer<FMSynth.Voice> voiceBuffer;

	private void OnGUI()
	{
		GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height) * 0.5f, new Vector2(Screen.width, Screen.height)), "Press A to play a sound");
	}

	void Awake()
	{
		mixer = GetComponent<Mixer>();
		synth = new FMSynth();
		voiceBuffer = new VoiceBuffer<FMSynth.Voice>(synth);
		channel = mixer.getChannel("Main");
		channel.addStage(synth);
		channel.setIsEnabled(true);
		mixer.setIsEnabled(true);

		voiceBuffer.initialize(5,
			voice => 
			{
				FMSynth.Voice.Operator op;

				// Let's setup some FM. First we create a modulator. They are sine waves by default, and has additive operation.
				op = voice.createAndAddOperator();
				// Set the envelope to be constant, and have a long release.
				op.envelope.setADSR(0, 0, 0, 100000);
				// Patch sound through the modulator pipeline.
				op.isModulator = true;
				// Set the output volume of the modulator. This directly affects the FM-index (and sidebands).
				op.volume = 0.3f;

				// Create the carrier. Again, it's just a additive sine wave by default.
				op = voice.createAndAddOperator();
				// Set the envelope to a 2 ms sharp attack, followed by a medium decay down to -64 dB.
				op.envelope.setADSR(2, 1000, -64, 1000);

				// Now we create a LP filter modulated by a LFO.
				// First setup the LFO, so it feeds into the filter.
				op = voice.createAndAddOperator();
				// Set a long attack on the LFO, and make it stay there (sustain is 0 dB)
				op.envelope.setADSR(1000, 0, 0, 10000);
				// Make it a modulator, again.
				op.isModulator = true;
				// Any operator has a harmonic relationship to the base pitch (which the synth decides).
				// This controlled through op.pitchRelation, and is 1 by default.
				// However, if we set op.isFixed to true, the pitch relation is ignored and assumes a fixed value.
				op.isFixed = true;
				// Since no relation is specified, we have to set the frequency (set through omega) ourselves.
				// The EnvironmentTuning specifies a set of variables related to tuning and tempo.
				// This one in particular is tied to the BPM, so we scale it to create a beating tempo-synced frequency.
				op.omega = EnvironmentTuning.bpmFreq * 2;
				// Modulations to filters is equal to cf * 2^(mod), so since the filters base frequency is 50,
				// the filter cutoff will modulate between 12.5 and 200 (volume scales modulations, of course)
				op.volume = 4f;
				// alter the starting phase of the modulator by 3/4.
				op.phaseOffset = Mathf.PI * 1.5f;

				// create the filter that recieves the modulation, with a cutoff of 50 hz, and fairly resonant (5)
				op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 50, 5);

				voice.setPitch(220);
			}
		);

	}

	void Start()
	{
		// start runs after awake. Enable the mixers sound system, and 
		mixer.enableSoundSystem();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			// steal the oldest voice, and play it!
			voiceBuffer.stealOldestVoice().play();
		}
	}
}