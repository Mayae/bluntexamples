﻿using UnityEngine;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;
using System.Collections;


public class AudioSystem_Modulation : MonoBehaviour
{
	Mixer mixer;
	Mixer.MixerChannel channel;
	FMSynth synth;
	VoiceBuffer<FMSynth.Voice> voiceBuffer;
	Utils.CheapRandom rnd = new Utils.CheapRandom();

	private void OnGUI()
	{
		GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height) * 0.5f, new Vector2(Screen.width, Screen.height)), "Press A to play a sound, and S to modulate it");
	}

	void Awake()
	{
		mixer = GetComponent<Mixer>();
		synth = new FMSynth();
		voiceBuffer = new VoiceBuffer<FMSynth.Voice>(synth);
		channel = mixer.getChannel("Main");
		channel.addStage(synth);
		channel.setIsEnabled(true);
		mixer.setIsEnabled(true);

		voiceBuffer.initialize(10,
			voice =>
			{
				FMSynth.Voice.Operator op;

				// Create the sine modulator, again, that we will scale using a parameter.
				op = voice.createAndAddOperator();
				op.envelope.setADSR(0, 0, 0, 100000);
				// lower the pitch and detune it slightly, to get a flanging effect.
				op.pitchRelation = 0.499f;
				op.isModulator = true;
				op.volume = 1f;

				// add a modulating parameter now, that will scale the volume of the modulator.
				// take note, this is the second operator we add - we need to index it later.
				op = voice.createAndAddParameter(0.0f);
				op.isModulator = true;
				// and make a wide range:
				op.volume = 2;
				// using multiplicative operation, we scale the previous modulator's volume.
				op.op = FMSynth.Voice.Operator.Operation.Multiplicative;

				// Create the carrier. Again, it's just a additive sine wave by default.
				op = voice.createAndAddOperator();
				// Set the envelope to a 2 ms sharp attack, followed by a medium decay down to -120 dB.
				op.envelope.setADSR(2, 7000, -120, 1000);


				// Add a filter.
				op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 200, 4);

				// Set the pitch of every voice to A2.
				voice.setPitch(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 2));
			}
		);

	}

	void Start()
	{
		// start runs after awake. Enable the mixers sound system, and 
		mixer.enableSoundSystem();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			// steal the oldest voice, and play it!
			voiceBuffer.stealOldestVoice().play();
		}
		else if (Input.GetKeyDown(KeyCode.S))
		{
			// generate a random target for the FM-index
			float rndTarget = rnd.random01();

			// set the parameter of each voice
			voiceBuffer.foreachVoice(
				voice =>
				{
					if (voice.enabled)
					{
					// interpolate the current paramater to the new target over 1500 miliseconds.
					voice.operators[1].parameterSetGoal(rndTarget, 1500);
					}
					else
					{
					// voices not currently playing is just set to the target directly.
					voice.operators[1].parameterSetValue(rndTarget);
					}
				}
			);
		}
	}
}