﻿using UnityEngine;
using Blunt;
using Blunt.Sound;
using System.Collections;


public class AudioSystem : MonoBehaviour
{
	Mixer mixer;
	Mixer.MixerChannel channel;

	void Awake()
	{
		mixer = GetComponent<Mixer>();
		// and, enable the mixer:
		mixer.setIsEnabled(true);
	}

	void Start()
	{
		// start runs after awake. Enable the mixers sound system, and 
		mixer.enableSoundSystem();
	}

}