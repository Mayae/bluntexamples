﻿using UnityEngine;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;
using Blunt.Sound.GenerativeMusic;
using System.Collections;


public class AudioSystem_Sequencer : MonoBehaviour
{
	Mixer mixer;
	Mixer.MixerChannel channel;
	FMSynth synth;
	VoiceBuffer<FMSynth.Voice> voiceBuffer;
	Utils.CheapRandom rnd = new Utils.CheapRandom();

	int[] majorScale = { 0, 2, 4, 5, 7, 9, 11 };

	// our callback sequencer, and token.
	CallbackSequencer<FMSynth> cbs;
	CallbackSequencer<FMSynth>.Token token;

	private void OnGUI()
	{
		GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height) * 0.5f, new Vector2(Screen.width, Screen.height)), "Press S to modulate the timbre of the sequencer");
	}

	void Awake()
	{
		mixer = GetComponent<Mixer>();
		synth = new FMSynth();
		voiceBuffer = new VoiceBuffer<FMSynth.Voice>(synth);
		channel = mixer.getChannel("Main");
		channel.addStage(synth);
		channel.setIsEnabled(true);
		mixer.setIsEnabled(true);

		// initiate the callback sequencer
		cbs = new CallbackSequencer<FMSynth>(synth);
		token = cbs.createCallbackToken(onSequencerCallback);
		token.callbackIn(PlayHead.TimeOffset.zero);

		voiceBuffer.initialize(10,
			voice =>
			{
				FMSynth.Voice.Operator op;

				// Create the sine modulator, again, that we will scale using a parameter.
				op = voice.createAndAddOperator();
				op.envelope.setADSR(0, 0, 0, 100000);
				// lower the pitch and detune it slightly, to get a flanging effect.
				op.pitchRelation = 0.499f;
				op.isModulator = true;
				op.volume = 1f;

				// add a modulating parameter now, that will scale the volume of the modulator.
				// take note, this is the second operator we add - we need to index it later.
				op = voice.createAndAddParameter(0.0f);
				op.isModulator = true;
				// and make a wide range:
				op.volume = 2;
				// using multiplicative operation, we scale the previous modulator's volume.
				op.op = FMSynth.Voice.Operator.Operation.Multiplicative;

				// Create the carrier. Again, it's just a additive sine wave by default.
				op = voice.createAndAddOperator();
				// Set the envelope to a 2 ms sharp attack, followed by a medium decay down to -120 dB.
				op.envelope.setADSR(2, 7000, -120, 1000);


				// Add a filter.
				op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 200, 4);

				// Set the pitch of every voice to A2.
				voice.setPitch(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 2));
			}
		);

	}

	// our callback function.
	void onSequencerCallback(CallbackSequencer<FMSynth>.Token token, PlayHead ph)
	{
		// the base pitch of our sound (D3)
		int baseNote = EnvironmentTuning.A4 - 7;
		// as said, we intent to alter the pitch each time a new note is played (which is what we're currently doing).
		// basically, we just select a random index in the majorScale array, which means we will get a random deviation distributed in the major scale of D.
		int transposition = majorScale[(int)(rnd.random01() * (majorScale.Length - 1))];
		// since we know we're at a quantizable beat (we started at zero), we don't have to quantize now - we can just select a 
		// random perfect beat offset between 1 and 4.
		PlayHead.TimeOffset offset = PlayHead.createTimeOffset(0, Mathf.RoundToInt(rnd.random01() * 3) + 1, 0, 0.0f);
		// we will also modulate the timbre of the sound using the previous method (random FM index)
		float rndTarget = rnd.random01();
		// collect the voice we will modulate
		FMSynth.Voice currentVoice;
		voiceBuffer.stealOldestVoice(out currentVoice);
		// alter the pitch
		currentVoice.setPitch(baseNote + transposition, 0.0f);
		// alter the timbre
		currentVoice.operators[1].parameterSetValue(rndTarget);
		// slightly alter the volume (in decibels, -24 to -12 dB) and play it.
		currentVoice.play(-18 + rnd.random11() * 6);
		// some debug info.
		print("Sequencing a sound " + transposition + " semitones away. Will be back in " + offset + ", currently at position: " + ph);
		// this is important - we will sequence the next callback dynamically between 1 and 4 beats away.
		token.callbackIn(offset);
	}

	void Start()
	{
		// start runs after awake. Enable the mixers sound system, and 
		mixer.enableSoundSystem();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			// steal the oldest voice, and play it!
			voiceBuffer.stealOldestVoice().play();
		}
		else if (Input.GetKeyDown(KeyCode.S))
		{
			// generate a random target for the FM-index
			float rndTarget = rnd.random01();

			// set the parameter of each voice
			voiceBuffer.foreachVoice(
				voice =>
				{
					if (voice.enabled)
					{
					// interpolate the current paramater to the new target over 1500 miliseconds.
					voice.operators[1].parameterSetGoal(rndTarget, 1500);
					}
					else
					{
					// voices not currently playing is just set to the target directly.
					voice.operators[1].parameterSetValue(rndTarget);
					}
				}
			);
		}
	}
}